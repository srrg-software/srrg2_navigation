include "hc204.inc"
include "hokuyo.inc"

define vespasiano position(
    size [0.35 0.45 0.2]
    origin [0 0 0 0]
    gui_nose 1
    drive "diff"
    
    # x y z qw
    hokuyo_utm30( pose [0.026684 0.000566467 -0.1 -0.0405333] )   
    hc204( pose [-0.108832 0.222869 -0.05 0.70533] )
    hc204( pose [-0.146814 -0.206277 -0.05 -0.70174] )
    hc204( pose [0.0513359  0.209558 -0.05  0.38115] )
    hc204( pose [0.0257088 -0.203147 -0.05 -0.38545] )
    hc204( pose [0.0539392  0.000411834 -0.05 0.0] )

    # Report error-free position in world coordinates
    #localization "gps"
    #localization_origin [ 0 0 0 0 ]

    # Some more realistic localization error
    localization "odom"
    #odom_error [ 0.0 0.0 0.0 0.1 ]
    odom_error [ 0.0 0.0 0.0 0.0 ]

)
