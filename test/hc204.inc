define hc204 ranger
(
  sensor( 			
    range [ 0.01  2.5 ]
    fov 30.0
    samples 1
  )

  # model properties
  color "green"
  size [ 0.05 0.05 0.05 ]

  block( points 4
	point[0] [0 0]
	point[1] [0 1]
	point[2] [1 1]
	point[3] [1 0]
	z [0 1]
  )

)

