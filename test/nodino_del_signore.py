#!/usr/bin/env python
# license removed for brevity
import rospy
from sensor_msgs.msg import Range
from sensor_msgs.msg import LaserScan

sonar0_pub = rospy.Publisher('robot_0/sonar_0', Range)
sonar1_pub = rospy.Publisher('robot_0/sonar_1', Range)
sonar2_pub = rospy.Publisher('robot_0/sonar_2', Range)
sonar3_pub = rospy.Publisher('robot_0/sonar_3', Range)
sonar4_pub = rospy.Publisher('robot_0/sonar_4', Range)

def publishCose(pub, value, header, frame_id):
    pcl_msg = Range()
    pcl_msg.header = header
    pcl_msg.header.frame_id = frame_id
    converted_value =  value / 2.72
    pcl_msg.range = converted_value
    pub.publish(pcl_msg)


def range0Callback(range_msg):
    publishCose(sonar0_pub, range_msg.ranges[0], range_msg.header, "robot_0/base_laser_link_1")
def range1Callback(range_msg):
    publishCose(sonar1_pub, range_msg.ranges[0], range_msg.header, "robot_0/base_laser_link_2")
def range2Callback(range_msg):
    publishCose(sonar2_pub, range_msg.ranges[0], range_msg.header, "robot_0/base_laser_link_3")
def range3Callback(range_msg):
    publishCose(sonar3_pub, range_msg.ranges[0], range_msg.header, "robot_0/base_laser_link_4")
def range4Callback(range_msg):
    publishCose(sonar4_pub, range_msg.ranges[0], range_msg.header, "robot_0/base_laser_link_5")


def listener():
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("/robot_0/base_scan_1", LaserScan, range0Callback)
    rospy.Subscriber("/robot_0/base_scan_2", LaserScan, range1Callback)
    rospy.Subscriber("/robot_0/base_scan_3", LaserScan, range2Callback)
    rospy.Subscriber("/robot_0/base_scan_4", LaserScan, range3Callback)
    rospy.Subscriber("/robot_0/base_scan_5", LaserScan, range4Callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
