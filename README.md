# srrg/thin_navigation package

# Running Localization and Planning w/ sonars

* `roslaunch rococo_bathroom.launch`
* `python nodino_del_signore.py`
* `roslaunch sonar_map_tf.launch`
* `roslaunch sonar_localizer.launch`
* `roslaunch sonar_planner.launch`
* `rosrun rviz rviz -d robot_0.rviz`

# Running Live Demo w/ orazio / vespasiano

* __run arduino 1/2 for robot and sonar__
* `rosrun srrg_core_ros srrg_state_publisher_node test/tiziano_robot_transform.txt`
* `roslaunch thin_navigation first_floor_vespasiano_live.launch`

In case of troubles, before cursing all the calendar, check the communication between rpi3 and laptop (is /odom published? Otherwise transform won't)

## Authors:

* Giorgio Grisetti
* Cristiano Gennari
* Tiziano Guadagnino
* Bartolomeo Della Corte

## Additional instructions ##

### thin_localizer ###

If you use the -gui flag a window shows the state of the localizer.

You can still control the localizer through ros messages (e.g. via rviz) in the usual way.

Move the robot with a joystick or some other teleoperation means.

Once the GUI starts, here are the keys to use:

    s: with this you toggle the set-pose mode.
       left click sets the robot position
       right click sets the robot orientation

    g: triggers global localization

    d: toggles the the distance map vs occupancy map view

    r: toggles particle resetting (they get resampled when entering in the unknown)
