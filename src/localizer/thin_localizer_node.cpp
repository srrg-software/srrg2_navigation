#include "ros_localizer.h"

using namespace std;
using namespace thin_navigation;


void startLocalizer(ROSLocalizer* localizer, ros::NodeHandle& private_nh) {
  double tmp;

  cerr << "Thin Localizer Parameters: " << endl;
  int particles;
  private_nh.param("particles", particles, 2000);
  cerr << "[int] _particles: " << particles << endl;

  double max_range;
  private_nh.param("max_range", max_range, 10.0);
  cerr << "[float] _max_range: " << max_range << endl;

  double min_weight;
  private_nh.param("min_weight", min_weight, 0.1);
  cerr << "[float] _min_weight: " << min_weight << endl;

  double distance_threshold;
  private_nh.param("distance_threshold", distance_threshold, 1.0);
  cerr << "[float] _distance_threshold: " << distance_threshold << endl;

  bool dynamic_restart;
  private_nh.param("dynamic_restart", dynamic_restart, false);
  cerr << "[bool] _dynamic_restart: " << dynamic_restart << endl;

  bool use_gui;
  private_nh.param("use_gui", use_gui, false);
  cerr << "[bool] _use_gui: " << use_gui << endl;

  std::string odom_frame_id;
  private_nh.param("odom_frame_id", odom_frame_id, std::string("odom"));
  localizer->setOdomFrameId(odom_frame_id);
  cerr << "[string] _odom_frame_id: " << odom_frame_id << endl;

  std::string base_frame_id;
  private_nh.param("base_frame_id", base_frame_id, std::string("base_link"));
  localizer->setBaseFrameId(base_frame_id);
  cerr << "[string] _base_frame_id: " << base_frame_id << endl;

  std::string global_frame_id;
  private_nh.param("global_frame_id", global_frame_id, std::string("map"));
  localizer->setGlobalFrameId(global_frame_id);
  cerr << "[string] _global_frame_id: " << global_frame_id << endl;

  std::string sonar_topic_0;
  private_nh.param("sonar_topic_0", sonar_topic_0, std::string("sonar_0"));
  cerr << "[string] _sonar_topic_0: " << sonar_topic_0 << endl;
  std::string sonar_topic_1;
  private_nh.param("sonar_topic_1", sonar_topic_1, std::string("sonar_1"));
  cerr << "[string] _sonar_topic_1: " << sonar_topic_1 << endl;
  std::string sonar_topic_2;
  private_nh.param("sonar_topic_2", sonar_topic_2, std::string("sonar_2"));
  cerr << "[string] _sonar_topic_2: " << sonar_topic_2 << endl;
  std::string sonar_topic_3;
  private_nh.param("sonar_topic_3", sonar_topic_3, std::string("sonar_3"));
  cerr << "[string] _sonar_topic_3: " << sonar_topic_3 << endl;
  std::string sonar_topic_4;
  private_nh.param("sonar_topic_4", sonar_topic_4, std::string("sonar_4"));
  cerr << "[string] _sonar_topic_4: " << sonar_topic_4 << endl;

  std::vector<std::string> sonar_topics;
  sonar_topics.push_back(sonar_topic_0);
  sonar_topics.push_back(sonar_topic_1);
  sonar_topics.push_back(sonar_topic_2);
  sonar_topics.push_back(sonar_topic_3);
  sonar_topics.push_back(sonar_topic_4);
  
  Eigen::Vector3d initial_pose(0,0,0);
  bool has_initial_pose=private_nh.hasParam("initial_pose_x");
  if (has_initial_pose) {
    private_nh.param("initial_pose_x", initial_pose.x(), 0.0);
    private_nh.param("initial_pose_y", initial_pose.y(), 0.0);
    private_nh.param("initial_pose_a", initial_pose.z(), 0.0);
    cerr << "[float] _initial_pose_x: " << initial_pose.x() << endl;
    cerr << "[float] _initial_pose_y: " << initial_pose.y() << endl;
    cerr << "[float] _initial_pose_a: " << initial_pose.z() << endl;
  } else {
    cerr << "[float] _initial pose_{x,y,a}: not set, starting global localization"<< endl;
  }
      

  localizer->init(particles, distance_threshold, 0.2, min_weight);
  if (use_gui)
    localizer->initGUI();

  localizer->setParticleResetting(dynamic_restart);
  localizer->subscribeCallbacks(sonar_topics);

  if  (has_initial_pose){
    localizer->setInitialPose(float(initial_pose[0]),float(initial_pose[1]),float(initial_pose[2]));
  } else 
    localizer->startGlobal();
}

int main(int argc, char **argv){
  ros::init(argc, argv, "thin_localizer_node");
  ros::NodeHandle n;
  ros::NodeHandle private_nh("~");


  //constructs the localizer
  ROSLocalizer* localizer=new ROSLocalizer(n);

  //requests the map
  localizer->requestMap();

  
  if (ros::ok())
    startLocalizer(localizer, private_nh);


  //run baby run
  ros::spin();
  return 0;
}
